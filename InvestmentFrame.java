package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
   A frame that shows the growth of an investment with variable interest.
*/
public class InvestmentFrame extends JFrame //setting window that appeared on the screen
{    
   private static final int FRAME_WIDTH = 450;
   private static final int FRAME_HEIGHT = 100;

   private static final double DEFAULT_RATE = 5;
   private static final double INITIAL_BALANCE = 1000;   

   private JLabel rateLabel;
   private JTextField rateField;
   private JButton button;
   private JLabel resultLabel;
   private JPanel panel;
   private String str = "";
   

   
   public InvestmentFrame()
   {  
 
      createTextField();
      createButton();
      createPanel();
      
      setVisible(true);
      setSize(FRAME_WIDTH, FRAME_HEIGHT);
   }
 
   private void createTextField() //Add text on window program
   {
      rateLabel = new JLabel("Interest Rate: ");
      resultLabel = new JLabel("balance");

      final int FIELD_WIDTH = 10;
      rateField = new JTextField(FIELD_WIDTH);
      rateField.setText("" + DEFAULT_RATE);
   }
   
   private void createButton()
   {
      button = new JButton("Add Interest");
      

   }

   public void setListener(ActionListener list) //add controller button
   {
 	  button.addActionListener(list);
   }
   private void createPanel() //add controller to use
   {
      panel = new JPanel();
      panel.add(rateLabel);
      panel.add(rateField);
      panel.add(button);
      panel.add(resultLabel);      
      add(panel);
   }
   
   public String getRate(){
		return rateField.getText();
	}
   
   
	public void setResult(String str) {
		resultLabel.setText(str+"");
	}
}
