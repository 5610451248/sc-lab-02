package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import model.BankAccount;     //'model'package.classname'BankAccount'
import gui.InvestmentFrame;   //'gui'package.classname'InvestmentFrame'

public class Controller {
	
	
	private static final double Startbalance = 1000; //set initial balance at 1000
	//If set to zero, nothing changed (0 * anything = 0)
	
	class ListenerMgr implements ActionListener {

		@Override
		
		// this method calculate the balance
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			//frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			//System.exit(0);
			
			
			double rate = Double.parseDouble(frame.getRate());
			double interest = account.getBalance()* rate / 100;
            account.deposit(interest);
            frame.setResult(""+account.getBalance());

		}

	}
	

	public static void main(String[] args) {
		
		new Controller();
	}

	public Controller() { //setting value
		frame = new InvestmentFrame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 100);	
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
	}
	double rate;
	private BankAccount account = new BankAccount(Startbalance);  
	ActionListener list;
	InvestmentFrame frame;
}